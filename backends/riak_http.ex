defmodule Beamie.Store.RiakHTTP do
  def init() do
    IO.puts "Initialized #{inspect __MODULE__}"
  end

  def shutdown() do
  end

  def put(bucket, key, value) do
    IO.puts "storing value #{inspect value} with key #{key} in bucket '#{bucket}'"
    put_req(bucket, key, value)
  end

  def get(bucket, key) do
    IO.puts "retrieving value for key #{key} from bucket '#{bucket}'"
    get_req(bucket, key)
  end

  ###

  @base_url "http://localhost:49160/"

  defp make_url(bucket, key) do
    (@base_url <> Path.join(["buckets", bucket, "keys", key]))
    |> :erlang.binary_to_list()
  end

  defp put_req(bucket, key, value) do
    content = pack_value(value)
    url = make_url(bucket, key)
    IO.inspect content
    IO.inspect url
    result = :httpc.request(:put, {url, [], 'text/plain', content}, [], [sync: true])
    case result do
      {:ok, {{_, status, _}, _headers, _data}=x} ->
        case status do
          204 -> :ok
          _   -> IO.inspect x; {:error, "backend error"}
        end
      {:error, reason}=t ->
        IO.inspect reason
        t
    end
  end

  defp get_req(bucket, key) do
    result = :httpc.request(:get, {make_url(bucket, key), []}, [], [sync: true, body_format: :binary])
    case result do
      {:ok, {{_, status, _}, _headers, data}} ->
        case status do
          200 -> {:ok, unpack_value(data)}
          _   -> nil
        end
      {:error, reason}=t ->
        IO.inspect reason
        t
    end
  end

  defp pack_value(term),  do: :erlang.term_to_binary(term)
  defp unpack_value(bin), do: :erlang.binary_to_term(bin)
end
