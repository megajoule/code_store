defmodule Beamie.Store.Dets do
  @table_name :"beamie_code_store"

  def init() do
    opts = [
      min_no_slots: 10000,
      type: :set,
      file: 'priv/db/#{@table_name}',
    ]
    {:ok, _} = :dets.open_file(@table_name, opts)
    IO.puts "Initialized #{inspect __MODULE__}"
  end

  def shutdown() do
    :dets.close(@table_name)
    IO.puts "Closed Dets table"
  end

  def put(bucket, key, value) do
    IO.puts "storing value with key #{key} in bucket '#{bucket}'"
    case :dets.insert(@table_name, {bucket<>key, value}) do
      :ok   -> :ok
      other -> other
    end
  end

  def get(bucket, key) do
    IO.puts "retrieving value for key #{key} from bucket '#{bucket}'"
    case :dets.lookup(@table_name, bucket<>key) do
      []               -> nil
      [{_, value}]     -> {:ok, value}
      {:error, reason} -> {:error, reason}
    end
  end
end
