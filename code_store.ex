defmodule Beamie.CodeStore do
  @moduledoc """
  A generic persistent key-value store, primarily meant for storing source code associated with
  hash-based identifiers.
  """

  @code_bucket   "code"
  @pastie_bucket "pastie"
  @pastie_cache_tab :pastie_cache_tab
  @pastie_cache_size 10

  @hash_algo :sha

  def init() do
    :ets.new(@pastie_cache_tab, [:public, :named_table, :set,
                              read_concurrency: true, write_concurrency: true])
    Beamie.Store.init()
  end

  def shutdown() do
    Beamie.Store.shutdown()
  end

  @doc """
  Persist `code` in the storage and return its hash.
  """
  def store_code(code) do
    key = hash_data(code)
    case Beamie.Store.put(@code_bucket, key, code) do
      :ok -> {:ok, key}
      other -> other
    end
  end

  @doc """
  Persist `code` and view parameters defined in `params` to the storage.
  Return public key of the pastie.
  """
  def store_pastie(params, code) do
    case store_code(code) do
      {:ok, code_key} ->
        value = {params, code_key}
        pkey = gen_pkey(value)
        case Beamie.Store.put(@pastie_bucket, pkey, value) do
          :ok   -> {:ok, pkey}
          other -> other
        end

      other -> other
    end
  end

  @doc """
  Retrieve pastie stored under key `pkey`.
  Return a tuple `{:ok, {params, code}}` if the key is found, otherwise `nil`.
  """
  def lookup_pastie(pkey) do
    if value = get_pastie_from_cache(pkey) do
      IO.puts "Got value for key #{pkey} from cache"
      {:ok, value}
    else
      case Beamie.Store.get(@pastie_bucket, pkey) do
        {:ok, value} ->
          processed = process_retrieved_value(value)
          store_pastie_in_cache(pkey, processed)
          {:ok, processed}
        other -> other
      end
    end
  end

  ###

  defp get_pastie_from_cache(key) do
    case :ets.lookup(@pastie_cache_tab, key) do
      []         -> nil
      [{_, val}] ->
        check_cache_size()
        val
    end
  end

  defp store_pastie_in_cache(key, value) do
    :ets.insert(@pastie_cache_tab, {key, value})
  end

  defp check_cache_size() do
    if :ets.info(@pastie_cache_tab, :size) > @pastie_cache_size do
      IO.puts "--> Too many objects in cache. Clearing cache"
      :ets.delete_all_objects(@pastie_cache_tab)
    end
  end

  defp hash_data(code) do
    :crypto.hash(@hash_algo, code)
    |> Base.url_encode64()
    # chop off the last byte because it will always be ?=
    |> drop_last_byte
  end

  defp drop_last_byte(bin), do: drop_last_bytes(bin, 1)
  defp drop_last_bytes(bin, n), do: :binary.part(bin, 0, byte_size(bin)-n)

  ###

  defp gen_pkey(data) do
    hash = :erlang.phash2(data, 0x100000000)  # 2^32
    Base.url_encode64(<<hash::32>>)
    # chop off the last two bytes because they will always be "=="
    |> drop_last_bytes(2)
  end

  ###

  defp process_retrieved_value({params, code_key}) do
    {:ok, code} = Beamie.Store.get(@code_bucket, code_key)
    {params, code, ""}
  end
end
