defmodule Beamie.Store.Generator do
  @moduledoc """
  Call `Beamie.Store.Generator.gen_module` before using any of the code store functions. It will
  generate a module `Beamie.Store` at runtime which will use the specified backend as its underlying
  implementation.
  """
  def gen_module(backend) do
    Module.create(Beamie.Store, quote do
      @backend unquote(backend)
      def init(),                  do: @backend.init()
      def shutdown(),              do: @backend.shutdown()
      def put(bucket, key, value), do: @backend.put(bucket, key, value)
      def get(bucket, key),        do: @backend.get(bucket, key)
    end)
  end
end
